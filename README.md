# Document Classifier

In this project a document is parsed into a set of sentences. Each sentence contains a list of phrases, which are made up of words.
Five different statistics are computed using this parsed document. The system is trained on a number of known
documents. Unknown documents are associated with similar documents using the closeness of the five computed statistics.

---

## AuthorFinder
Main driver class of the lab whose job it is to make a reasonable "guess" as to
  who the author of a mystery text is based on statistics files from known authors
 
## Document
Class designed for parsing a document
  Using recursive top-down parsing consisting of tokens, phrases, and sentences
  The document has a list of sentences

##  DocumentStatistics
 Creates five different statistics, using the already parsed document from the Document class to do so:
  The five statistics are the average word length, the type token ratio, the hapax legomana ratio,
  the average words per sentence, and the sentence complexity of the parsed document

##  Phrase
 A phrase is a list of tokens ending at the end of phrase, end of sentence,
  or end of file tokens

##  Scanner
 A Scanner is responsible for reading an input stream, one character at a
  time, and separating the input into tokens.  A token is defined as:
   1. A 'word' which is defined as a non-empty sequence of characters that 
      begins with an alpha character and then consists of alpha characters, 
      numbers, the single quote character "'", or the hyphen character "-". 
   2. An 'end-of-sentence' delimiter defined as any one of the characters 
      ".", "?", "!".
   3. An end-of-file token which is returned when the scanner is asked for a
      token and the input is at the end-of-file.
   4. A phrase separator which consists of one of the characters ",",":" or
      ";".
   5. A digit.
   6. Any other character not defined above.

##  Sentence
 Creates the sentence object which is a list of phrases terminated
  by an end of sentence or end of file marker

##  Token
 A token is has a type and a value
  Can be a word, digit, special character, end of phrase marker, end of sentence marker, or 
  end of file marker