import java.io.IOException;
import java.io.Reader;

/**
 * A Scanner is responsible for reading an input stream, one character at a
 * time, and separating the input into tokens.  A token is defined as:
 *  1. A 'word' which is defined as a non-empty sequence of characters that 
 *     begins with an alpha character and then consists of alpha characters, 
 *     numbers, the single quote character "'", or the hyphen character "-". 
 *  2. An 'end-of-sentence' delimiter defined as any one of the characters 
 *     ".", "?", "!".
 *  3. An end-of-file token which is returned when the scanner is asked for a
 *     token and the input is at the end-of-file.
 *  4. A phrase separator which consists of one of the characters ",",":" or
 *     ";".
 *  5. A digit.
 *  6. Any other character not defined above.
 * @author Mr. Page
 * @author Ayush Alag
 * @version February 7, 2017
 * 
 *
 */

public class Scanner
{
    private Reader in;
    private String currentChar;
    private boolean endOfFile;
    // define symbolic constants for each type of token
    public static enum TOKEN_TYPE{WORD, END_OF_SENTENCE, END_OF_FILE, 
        END_OF_PHRASE, DIGIT, UNKNOWN};
    /**
     * Constructor for Scanner objects.  The Reader object should be one of
     *  1. A StringReader
     *  2. A BufferedReader wrapped around an InputStream
     *  3. A BufferedReader wrapped around a FileReader
     *  The instance field for the Reader is initialized to the input parameter,
     *  and the endOfFile indicator is set to false.  The currentChar field is
     *  initialized by the getNextChar method.
     * @param in is the reader object supplied by the program constructing
     *        this Scanner object.
     */
    public Scanner(Reader in)
    {
        this.in = in;
        endOfFile = false;
        getNextChar();
    }
    /**
     * The getNextChar method attempts to get the next character from the input
     * stream.  It sets the endOfFile flag true if the end of file is reached on
     * the input stream.  Otherwise, it reads the next character from the stream
     * and converts it to a Java String object.
     * postcondition: The input stream is advanced one character if it is not at
     * end of file and the currentChar instance field is set to the String 
     * representation of the character read from the input stream.  The flag
     * endOfFile is set true if the input stream is exhausted.
     */
    private void getNextChar()
    {
        try
        {
            int inp = in.read();
            if(inp == -1) 
                endOfFile = true;
            else 
                currentChar = "" + (char) inp;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
    }
    
    /**
     * If the current char is equal to the char being read in, 
     * it takes in the next character; otherwise, it throws an exception
     * @param s the string being checked against current char
     */
    private void eat(String s)
    {
    	if (currentChar.equals(s))
    		getNextChar();
    	else
    		throw new IllegalArgumentException();
    }
    /**
     * Checks if the string is a letter
     * @param s the string that was passed in
     * @return returns true if it is a letter, false otherwise
     */
    private boolean isLetter(String s)
    {
    	return (s.toLowerCase().compareTo("a")>=0 && s.toLowerCase().compareTo("z")<=0);
    }
    /**
     * Checks if the string is a digit
     * @param s the string that was passed in
     * @return returns true if it is a digit, false otherwise
     */
    private boolean isDigit (String s)
    {
    	return (s.compareTo("0")>=0 && s.compareTo("9")<=0);
    }
    /**
     * Checks if the string is a special character (single quote or hyphen)
     * @param s the string that was passed in
     * @return returns true if it is a special character, false otherwise
     */
    private boolean isSpecialChar(String s)
    {
    	return (s.equals("'") || s.equals("-"));
    }
    /**
     * Checks if the string is a phrase terminator (comma, colon, semicolon)
     * @param s the string that was passed in
     * @return returns true if it is a phrase terminator, false otherwise
     */
    private boolean isPhraseTerminator (String s)
    {
    	return (s.equals(",") || s.equals(";") || s.equals(":"));
    }
    /**
     * Checks if the string is a sentence terminator (comma, colon, semicolon)
     * @param s the string that was passed in
     * @return returns true if it is a sentence terminator, false otherwise
     */
    private boolean isSentenceTerminator (String s)
    {
    	return (s.equals(".") || s.equals("!") || s.equals("?"));
    }
    /**
     * Checks if the string is white space
     * @param s the string that was passed in
     * @return true if it is a sentence terminator, false otherwise
     */
    private boolean isWhiteSpace (String s)
    {
    	return (s.equals(" ") || s.equals("\n") || s.equals("\t"));
    }
    /**
     * Checks whether there are more tokens that can be returned
     * @return true if there are more tokens, false otherwise
     */
    public boolean hasNextToken()
    {
    	return !endOfFile;
    }
    
    /**
     * Retrieves the next token
     * If there are white spaces, they are omitted
     * The word token is returned if it starts with an alphabet and goes while
     * the characters are either special characters, letters, or digits
     * If the digit is found first, it is returned as a token digit
     * If sentence and phrase terminators, they are returned as tokens of that type
     * Otherwise, whatever else is left is returned as unknown token types
     * Unless it is the end of the document, where an end of file token is returned
     * @return Token the next token object in the string that is being parsed
     */
    public Token nextToken()
    {
    	if (hasNextToken())
    	{
    		while (hasNextToken() && isWhiteSpace(currentChar))
    		{
    			eat(currentChar);
    		}
    		if (isLetter(currentChar))
    		{
    			String returning = "";
    			while(hasNextToken() && (isLetter(currentChar) || isSpecialChar(currentChar) || isDigit(currentChar)))
    			{
    				returning = returning + currentChar;
    				eat(currentChar);
    			}
    			String r = returning.toLowerCase();
    			return new Token(TOKEN_TYPE.WORD, r);
    		}

    		if (isDigit(currentChar))
    		{
    			String c = currentChar;
    			eat(currentChar);
    			return new Token(TOKEN_TYPE.DIGIT, c);
    		}
    		if (isSpecialChar(currentChar))
    		{
    			String c = currentChar;
    			eat(currentChar);
    			return new Token(TOKEN_TYPE.UNKNOWN, c);
    		}
    		if (isPhraseTerminator(currentChar))
    		{
    			String c = currentChar;
    			eat(currentChar);
    			return new Token(TOKEN_TYPE.END_OF_PHRASE, c);
    		}
    		if (isSentenceTerminator(currentChar))
    		{
    			String c = currentChar;
    			eat(currentChar);
    			return new Token(TOKEN_TYPE.END_OF_SENTENCE, c);
    		}
    		
    		if (hasNextToken())
    		{
    			String c = currentChar;
    			eat(currentChar);
    			return new Token(TOKEN_TYPE.UNKNOWN, c);
    		}
    	}
	return new Token(TOKEN_TYPE.END_OF_FILE, "");
    }   
}
