import java.util.ArrayList;
import java.util.List;

/**
 * Creates the sentence object which is a list of phrases terminated
 * by an end of sentence or end of file marker
 * @author AyushAlag
 * @version February 15, 2017
 *
 */
public class Sentence 
{
	private List<Phrase> phrases;
	
	/**
	 * Instantiates the instance variable to a new array list
	 */
	public Sentence()
	{
		phrases = new ArrayList<Phrase>();
	}
	
	/**
	 * Adds the token to the instance variable which is a list of phrases
	 * Runs in O(1) time
	 * @param p the phrase being added to the list
	 */
	public void addToList(Phrase p)
	{
		phrases.add(p);
	}
	
	/**
	 * Creates a copy of the instance variable which is the list of phrases this sentence object
	 * possesses
	 * @return the List<Phrase> copy created
	 */
	public List<Phrase> copy()
	{
		List<Phrase> newList = new ArrayList<Phrase>();
		for (Phrase p: phrases)
		{
			newList.add(p);
		}
		return newList;
	}
	
	/**
	 * To string method that formats the sentence appropriately, displaying all of the
	 * phrase objects (which are already converted to string, displaying token objects)
	 * in the sentence and separating them with spaces
	 * @return the String version of the object
	 */
	public String toString()
	{
		String print = "";
		for (Phrase p: phrases)
		{
			print = print + (p.toString() + " ");
		}
		return print.substring(0, print.length()-1);
	}
	
}
