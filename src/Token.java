/**
 * A token is has a type and a value
 * Can be a word, digit, special character, end of phrase marker, end of sentence marker, or 
 * end of file marker
 * @author AyushAlag
 * @version February 15, 2017
 *
 */
public class Token 
{
	private final Scanner.TOKEN_TYPE TYPE;
	private String s;
	
	/**
	 * Instantiates the instance variables to the corresponding variables passed in
	 * @param T the type of the token being set
	 * @param str the String value of the token
	 */
	public Token(Scanner.TOKEN_TYPE T, String str)
	{
		TYPE = T;
		s = str;
	}
	
	/**
	 * Returns the type of the token
	 * @return the Scanner.TOKEN_TYPE type of the token
	 */
	public Scanner.TOKEN_TYPE getType()
	{
		return TYPE;
	}
	
	/**
	 * Returns the value of the token
	 * @return the string value of the token
	 */
	public String getValue()
	{
		return s;
	}
	
	/**
	 * To string method that displays the type and value of the token
	 * @return the String version of the token
	 */
	public String toString()
	{
		return "Type: " + TYPE + ", Value: " + s;
	}
	
	/**
	 * Checks whether two tokens are equal by comparing their types and values
	 * Alternative solution is put in comments that utilizes the hashcode method
	 * @param t the token being compared to this one
	 * @return true if the two tokens are equal, false otherwise
	 */
	public boolean equals(Token t)
	{
		return (TYPE == t.getType() && s == t.getValue());
		//return t.hashCode() == hashCode(); alternative solution
	}
	
	/**
	 * Returns the hashcode of the toString version of the object
	 * Can be used in the equals method
	 * @return the int hashcode of the toString version of the object
	 */
	public int hashCode()
	{
		return (toString().hashCode());
	}
}
