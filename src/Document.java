import java.util.*;

/**
 * Class designed for parsing a document
 * Using recursive top-down parsing consisting of tokens, phrases, and sentences
 * The document has a list of sentences
 * @author AyushAlag
 * @version February 15, 2017
 *
 */
public class Document 
{
	private Scanner s;
	private List<Sentence> sentences;
	private Token t;
	
	/**
	 * This constructor instantiates the scanner to the passed in scanner object
	 * Also sets the list of sentence objects to a new array list of sentence objects
	 * Calls getNextToken and parseDocument
	 * The end result is that the document passed in is parsed and the sentences are filled
	 * @param scan the scanner being passed in which the instance variable is being set to
	 */
	public Document(Scanner scan)
	{
		s = scan;
		sentences = new ArrayList<Sentence>();
		getNextToken();
		parseDocument();
	}
	
	/**
	 * Sets the token
	 *  instance variable to the next token retrieved by the scanner class
	 */
	public void getNextToken()
	{
		t = s.nextToken();
	}
	
	/**
	 * Takes in the next token using the getNextToken() method
	 * Also before this checks if the two tokens are the same (the param) and the instance variable
	 * Which will always be true in this lab
	 * If it was not true, a runtime exception would be thrown
	 * @param tok the token being checked with the instance variable
	 */
	public void eat(Token tok)
	{
		if (tok.equals(t))
		{
			getNextToken();
		}
		else
		{
			throw new RuntimeException();
		}
	}
	
	/**
	 * Creates a phrase object, adding tokens to it until an end of phrase/sentence/file token is reached
	 * @return the Phrase object that was created and filled with the tokens of the phrase
	 */
	public Phrase parsePhrase()
	{
		Phrase p = new Phrase();
		while (!t.getType().equals(Scanner.TOKEN_TYPE.END_OF_PHRASE) && !t.getType().equals(Scanner.TOKEN_TYPE.END_OF_FILE) && !t.getType().equals(Scanner.TOKEN_TYPE.END_OF_SENTENCE))
		{
			if (t.getType().equals(Scanner.TOKEN_TYPE.WORD))
			{
				p.addToList(t);
			}
			eat(t);
		}
		if (t.getType().equals(Scanner.TOKEN_TYPE.END_OF_PHRASE))
		{
			eat(t);
		}
		return p;
	}
	
	/**
	 * Creates a sentence object and adds phrases to it until an end of sentence/file
	 * marker is reached
	 * Calls the parsePhrase() method when adding phrases to the sentence
	 * @return the sentence object
	 */
	public Sentence parseSentence()
	{
		Sentence s = new Sentence();
		while(t.getType()!=Scanner.TOKEN_TYPE.END_OF_SENTENCE && t.getType()!=Scanner.TOKEN_TYPE.END_OF_FILE)
		{
			s.addToList(parsePhrase());
		}
		if (t.getType().equals(Scanner.TOKEN_TYPE.END_OF_SENTENCE))
		{
			eat(t);
		}
		return s;
	}
	
	/**
	 * Parses the document, adding Sentence objects to the instance variable which is a list of sentences
	 * Until an end of file marker is reached
	 */
	public void parseDocument()
	{
		while(!t.getType().equals(Scanner.TOKEN_TYPE.END_OF_FILE))
		{
			sentences.add(parseSentence());
		}		
	}
	
	/**
	 * Returns the instance variable which is the list of sentences
	 * @return List<Sentence> the variable containing the various Sentences that were parsed
	 */
	public List<Sentence> getSentences()
	{
		return sentences;
	}
}
