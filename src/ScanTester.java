import java.io.StringReader;

/**
 * Creates a tester that tests to see if the scanner class works and if the string is tokenized correctly
 * Prints out all of the tokens in the test string
 * @author AyushAlag
 * @version February 7, 2017
 *
 */
public class ScanTester {

	public static void main(String[] args) 
	{
		StringReader reader = new StringReader("this is the\nfirst sentence. Isn't\nit?"
				+ " Yes ! !! This \n\nlast bit :) is als3o a  4  4 se4ntence, "
				+ "but \nwit1hout a terminator other than the end of the file \n"
				+ "this is the \n first sentence.");
		Scanner scanner = new Scanner(reader);
		while(scanner.hasNextToken())
		{
			System.out.println(scanner.nextToken());
		}
	}

}
