import java.util.*;
/**
 * 
 * A phrase is a list of tokens ending at the end of phrase, end of sentence,
 * or end of file tokens
 * @author AyushAlag
 * @version February 15, 2017
 * ArrayList chosen because it has quick retrieval and addition of elements
 */
public class Phrase 
{
	private List<Token> tokens;
	
	/**
	 * Instantiates the instance variable to a new array list
	 */
	public Phrase()
	{
		tokens = new ArrayList<Token>();
	}
	
	/**
	 * Adds the token to the instance variable which is a list of tokens
	 * Runs in O(1) time
	 * @param t the token being added
	 */
	public void addToList(Token t)
	{
		tokens.add(t);
	}
	
	/**
	 * Creates a copy of the instance variable which is the list of tokens this phrase object
	 * possesses
	 * @return the List<Token> copy created
	 */
	public List<Token> copy()
	{
		List<Token> newList = new ArrayList<Token>();
		for (Token t: tokens)
		{
			newList.add(t);
		}
		return newList;
	}
	
	/**
	 * To string method that formats the phrase appropriately, displaying all of the
	 * tokens in the phrase and separating them with spaces
	 * @return the String version of the object
	 */
	public String toString()
	{
		String print = "";
		String space = " ";
		int tokCount = 0;
		for (Token t: tokens)
		{
			if (tokCount>0 && (!t.getType().equals(Scanner.TOKEN_TYPE.END_OF_PHRASE)) && (!t.getType().equals(Scanner.TOKEN_TYPE.END_OF_SENTENCE)))
			{
				print = print + (space + t.getValue());
				tokCount++;
			}
			else
			{
				print = print + (t.getValue());
				tokCount++;
			}
		}
		return print;
	}
	
}
