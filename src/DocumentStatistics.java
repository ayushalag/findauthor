import java.util.*;
/**
 * Creates five different statistics, using the already parsed document from the Document class to do so:
 * The five statistics are the average word length, the type token ratio, the hapax legomana ratio,
 * the average words per sentence, and the sentence complexity of the parsed document
 * @author AyushAlag
 * @version February 15, 2017
 *
 */
public class DocumentStatistics 
{
	private Document d;
	
	/**
	 * Sets the instance variable Document object to the passed in Document object
	 * @param doc the Document to which the instance variable is being set to
	 */
	public DocumentStatistics(Document doc)
	{
		d = doc;
	}
	
	/**
	 * This is the average number of characters per word, calculated after 
	 * the punctuation has been stripped. The comma and the final period are stripped 
	 * but any hyphen characters or underscore characters are counted.
	 * Two variables are created: a counter that counts the number of words, and one that counts
	 * the total number of characters
	 * Then, the instance variable Document's sentences are retrieved and are broken down into phrases
	 * and tokens
	 * For each token, the number of words is incremented by one and the number of characters is incremented
	 * by the length of the value of the token
	 * The number of characters is then divided by the number of words
	 * @return double average characters per word
	 */
	public double getAverageWordLength()
	{
		int numChars = 0;
		int numWords = 0;
		List<Sentence> sentences = d.getSentences();
		for (Sentence s: sentences)
		{
			for (Phrase p: s.copy())
			{
				for (Token t: p.copy())
				{
					numChars += t.getValue().length();
					numWords++;
				}
			}
		}
		return ((double)numChars)/numWords;
	}
	
	/**
	 * This is the number of different words used in a text divided by the total number of words
	 * First a set is created as well as a counter that counts the number of words in the document
	 * Like the above method, this splits up the document object into sentences, phrases, and tokens,
	 * and then for each token, it increments the number of words and then adds this token to the set
	 * Since a set cannot take duplicate values, the set will automatically each word only once,
	 * thereby showing the number of different words used in the text document
	 * Finally, the size of the set is divided by the number of total words in the document to get 
	 * this ratio
	 * @return the double type token ratio
	 */
	public double getTypeTokenRatio()
	{
		Set<String> tokens = new HashSet<String>();
		double numWords = 0.0;
		for (Sentence s: d.getSentences())
		{
			for (Phrase p: s.copy())
			{
				for (Token t: p.copy())
				{
					numWords++;
					tokens.add(t.getValue());
				}
			}
		}
		return (tokens.size())/numWords;
	}
	
	/**
	 * This ratio is the number of words occurring exactly once in the text divided by
	 * the total number of words in the text.
	 * First, counter variables for unique and total values are created
	 * Then, a map which contains a string as the key and a hashset of strings as the value is created
	 * As before, the sentences from the document object that is the instance variable are extracted
	 * These sentences are then broken up into phrases and tokens
	 * For each token:
	 * The map is first checked if it contains that token's value as its key
	 * If it does not, the token's value (a string holding the word) is put inside the map as the key
	 * with a corresponding value of null
	 * If the map already contains the token's value, then
	 * this means that this word (token's value) was already in the document before
	 * and to show this, this token's value gets added to the set which is the value of the map variable
	 * Thus, all of the unique words will have null corresponding values in the map since they
	 * only appeared once in the document
	 * Also, the number of total words is incremented for each token
	 * Then, to check this, the keys are extracted from the map and an iterator is used to iterate
	 * through all of the keys
	 * For each key, the corresponding value is checked
	 * If the value is null, this means that the key is a unique value (word) and the number of uniques
	 * is incremented
	 * Otherwise, the number of uniques are not incremented (since the word appeared more than once)
	 * Finally, the number of unique words over the number of total words is calculated, and this
	 * is the hapax legomana ratio we were looking for
	 * @return double the hapax legomana ratio
	 */
	public double getHapaxLegomanaRatio()
	{
		double uniques = 0;
		double total = 0;
		Map<String, HashSet<String>> tokens = new HashMap<String, HashSet<String>>();
		for (Sentence s: d.getSentences())
		{
			for (Phrase p: s.copy())
			{
				for (Token t: p.copy())
				{
					if (tokens.containsKey(t))
					{
						tokens.get(t.getValue()).add(t.getValue());
					}
					else
					{
						tokens.put(t.getValue(), null);
					}
					total++;
				}
			}
		}
		Set<String> s = tokens.keySet();
		Iterator it = s.iterator();
		while(it.hasNext())
		{
			Object key = it.next();
			if (tokens.get(key)==null)
				uniques++;
		}
		return uniques/total;
	}
	
	/**
	 * This measures the average amount of words per sentence in the text file
	 * First, a counter for the number of total words is created
	 * Then, the number of sentences is found by calling the getSentences() method on the instance variable
	 * which is a document object and calling the size method on this arraylist
	 * Then, these sentences are broken up into phrases and tokens, and for each token, the number of
	 * words counter is incremented
	 * Finally, the number of words is divided by the number of sentences and this ratio is returned
	 * @return double the number of words per sentence
	 */
	public double getAverageWordsPerSentence()
	{
		int numWords = 0;
		List<Sentence> sentences = d.getSentences();
		int numSentences = sentences.size();
		for (Sentence s: sentences)
		{
			for (Phrase p: s.copy())
			{
				for (Token t: p.copy())
				{
					numWords++;
				}
			}
		}
		return ((double)numWords)/numSentences;
	}
	
	/**
	 * Gets the sentence complexity, meaning the average number of phrases per sentence
	 * As before, gets the number of sentences from the document instance variable
	 * Sets up a variable that keeps track of the number of phrases in the text file
	 * Then, breaks down the sentences into phrases and increments the phrase counter for each phrase
	 * Finally, the number of phrases are divided by the number of sentences
	 * @return double the number of phrases divided by the number of sentences (sentence complexity)
	 */
	public double getSentenceComplexity()
	{
		int numPhrases = 0;
		int numSentences = d.getSentences().size();
		for (Sentence s: d.getSentences())
		{
			for (Phrase p: s.copy())
			{
				numPhrases++;
			}
		}
		return ((double)numPhrases)/numSentences;
	}
}
