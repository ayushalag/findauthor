import java.io.*;
import java.util.*;
/**
 * Main driver class of the lab whose job it is to make a reasonable "guess" as to
 * who the author of a mystery text is based on statistics files from known authors
 * @author AyushAlag
 * @version February 15, 2017
 *
 */
public class AuthorFinder
{
	private Map<String, Map<String, String>> authorStatistics = new HashMap<String, Map<String, String>>();
	
	/**
	 * For each statistic file retrieves the name and ratios and inserts this into statistics
	 * First, it creates an array of files where each file is a statistic file for each author
	 * Then, it creates a new map called which has a string for a key and a value
	 * It reads in the first line of the file, which has the author's name, and stores this in a variable
	 * It then reads the next few lines, which are the ratios
	 * It inserts into the local map variable the name of the ratio as the key and its corresponding ratio as the value
	 * After all of the ratios are inputted into this local map, this entire local map is put
	 * into the instance variable,
	 * which takes a string as the key and another map (of key type string and value type string) as the value
	 * Thus, the author's name which was stored in the instance variable is the key and the ratio map is put in as the value
	 * This process is repeated for every file containing the different author's statistics
	 * @postcondition the instance variable contains all of the author's names as the key and their corresponding statistics as the values
	 * @throws IOException
	 */
	public void getStats() throws IOException
	{
		File [] files = new File("/Users/AyushAlag/Downloads/FindAuthorMaterial/SignatureFiles").listFiles();
		for (int i = 0; i<files.length; i++)
		{
				BufferedReader br = new BufferedReader(new FileReader(files[i]));
				Map<String, String> ratios = new HashMap<String, String>();
				String author = br.readLine();
				ratios.put("Avg word Length", br.readLine());
				ratios.put("Type Token Ratio", br.readLine());
				ratios.put("Hapax Legomana", br.readLine());
				ratios.put("Avg words per sentence", br.readLine());
				ratios.put("Sentence Complexity", br.readLine());
				authorStatistics.put(author, ratios);
		}
	}
	
	/**
	 * Takes in the mystery text passed in by other methods and returns the document statistics
	 * Associated with it
	 * Reads this file with a buffered reader
	 * Creates a scanner which takes in the buffered reader, a document which takes in the scanner
	 * And finally a DocumentStatistics object which takes in the document
	 * @param f the file that contains the mystery text whose author we are trying to find
	 * @return the document statistics object associated with the mystery file
	 * @throws IOException
	 */
	public DocumentStatistics mysteryStatsFinder(File f) throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(f));
		Scanner scan = new Scanner(br);
		Document d = new Document(scan);
		DocumentStatistics stats = new DocumentStatistics(d);
		return stats;
	}
	
	/**
	 * Creates a new map which has a string as a key and a double as a value
	 * Extracts the keys(author names) from the authorStatistics instance variable
	 * Creates and iterator on the set of keys. For each author, the map of ratios for that particular
	 * author is extracted from the authorStatistics instance variable
	 * Then, for each ratio, the difference between the known author's ratio and the mystery ratio 
	 * is computed and weighted using a helper method
	 * After this process has been repeated for all of the ratios, the all of the weighted differences
	 * are added together and this total is found
	 * A map entry with the key being the author whose ratios are being compared to the mystery
	 * And the value as the total difference is put in the map
	 * Finally, a helper method is called that goes through the map and finds the smallest total
	 * ratio difference
	 * The author who is associated with that difference is returned, and the "guess" for the mystery author
	 * 
	 * @param d the documentstatistics object (returned by mysteryStatsFinder) 
	 * that receives the ratios for the mystery text
	 * 
	 * @return String the author who's writing style is closest to that of the mystery text
	 */
	public String findAuthor(DocumentStatistics d)
	{
		Set<String> authors = authorStatistics.keySet();
		Map<String, Double> mysteryDifferences = new HashMap<String, Double>();
		Iterator it = authors.iterator();
		while (it.hasNext())
		{
			Object author = it.next();
			Map<String, String> stats = authorStatistics.get(author);
			String avgWordLength = stats.get("Avg word Length");
			double wordDiff = computeDifference(avgWordLength, d.getAverageWordLength(), 11);
			String typeToken  = stats.get("Type Token Ratio");
			double tokenDiff = computeDifference(typeToken, d.getTypeTokenRatio(), 33);
			String hapaxLegomana = stats.get("Hapax Legomana");
			double hapaxDiff = computeDifference(hapaxLegomana, d.getHapaxLegomanaRatio(), 50);
			String avgWordSentence = stats.get("Avg words per sentence");
			double wordSentenceDiff = computeDifference(avgWordSentence, d.getAverageWordsPerSentence(), 0.4);
			String sentenceCompl = stats.get("Sentence Complexity");
			double complexDiff = computeDifference(sentenceCompl, d.getSentenceComplexity(), 4);
			double totalDiff = wordDiff + tokenDiff + hapaxDiff + wordSentenceDiff + complexDiff;
			mysteryDifferences.put((String)author, totalDiff);
		}
		//System.out.println(mysteryDifferences); for debugging
		return findSmallestDifferenceAuthor(mysteryDifferences);
	}
	
	
	/**
	 * Helper method which computes the weighted difference between the ratios
	 * First, it converts the author's statistics which was passed in to a double, and finds
	 * the absolute value of the difference between this and the ratio found in the mystery file
	 * Then, it multiplies this by the weight and returns the weighted difference
	 * @param authorStat the String ratio of the author which is being checked
	 * @param mystStat the ratio of the mystery file
	 * @param weight the specific weight for that ratio
	 * @return the weighted difference
	 */
	private double computeDifference(String authorStat, double mystStat, double weight)
	{
		double rawDiff = Math.abs(Double.parseDouble(authorStat)-mystStat);
		return rawDiff*weight;
	}
	
	/**
	 * First, it extracts the author set from the map and creates an iterator
	 * It sets the smallest value to the max value of a double
	 * For each author, it checks whether the calculated total difference for that author is less
	 * than the smallest
	 * If so, then it replaces the smallest with this total difference and replaces the smallest author
	 * variable (which was before instantiated to an empty string) to the current author's name
	 * Basically, a classic minimum finder algorithm
	 * @param m the map which contains the author's names mapped to their total weighted differences between the author's
	 * statistics and the mystery statistics
	 * @return String the name of the author with the smallest total difference
	 */
	private String findSmallestDifferenceAuthor(Map<String, Double> m)
	{
		Set<String> authors = m.keySet();
		double smallest = Double.MAX_VALUE;
		String smallestAuthor = "";
		Iterator it = authors.iterator();
		while(it.hasNext())
		{
			String author = (String)it.next();
			if (m.get(author)<smallest)
			{
				smallest = m.get(author);
				smallestAuthor = author;
			}	
		}
		return smallestAuthor;
	}
	
	/**
	 * Creates a new author finder class, and then calls getStats to 
	 * fill the instance variable author stats
	 * Then for each mystery file, it prints out the file name plus the name of the author who it thinks
	 * wrote that file
	 * This is done by passing in the file and calling mysteryStatsFinder() on it, which returns a document statistics object
	 * and then calling the findAuthor() method with that documentstatistics object passed in, which returns the name
	 * of the smallest author
	 * @throws IOException
	 */
	public static void main (String [] args) throws IOException
	{
		AuthorFinder a = new AuthorFinder();
		a.getStats();
		System.out.println("mystery1: " + a.findAuthor(a.mysteryStatsFinder(new File("/Users/AyushAlag/Downloads/FindAuthorMaterial/MysteryText/mystery1.txt"))));
		System.out.println("mystery2: " + a.findAuthor(a.mysteryStatsFinder(new File("/Users/AyushAlag/Downloads/FindAuthorMaterial/MysteryText/mystery2.txt"))));
		System.out.println("mystery3: " + a.findAuthor(a.mysteryStatsFinder(new File("/Users/AyushAlag/Downloads/FindAuthorMaterial/MysteryText/mystery3.txt"))));
		System.out.println("mystery4: " + a.findAuthor(a.mysteryStatsFinder(new File("/Users/AyushAlag/Downloads/FindAuthorMaterial/MysteryText/mystery4.txt"))));
		System.out.println("mystery5: " + a.findAuthor(a.mysteryStatsFinder(new File("/Users/AyushAlag/Downloads/FindAuthorMaterial/MysteryText/mystery5.txt"))));
	}
	
}