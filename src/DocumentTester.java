import java.io.StringReader;
import java.util.*;
import java.io.*;
/**
 * Tests to make sure the document class works and parses the passed in "document" correctly
 * @author AyushAlag
 * @version February 15, 2017
 *
 */
public class DocumentTester 
{
	public static void main (String[] args)
	{
		StringReader s = new StringReader(""
				+ "t2his is the first! sentence hello s s s s;"
				+ " and there are mor.e to come 23 2. "
				+ "This is t\nhe next sentence, aNd th-eere may be more to come after this     "
				+ "but we don't know,");
		Scanner scan = new Scanner(s);
		Document d = new Document (scan);
		List<Sentence> l = d.getSentences();
		for (Sentence se: l)
		{
			System.out.println(se);
		}
		
	}
}
